/*
 * httpd_server.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef HTTPD_SERVER_H
#define HTTPD_SERVER_H


extern "C" void taskHTTPServer(void*);

#endif /* HTTPD_SERVER_H */

