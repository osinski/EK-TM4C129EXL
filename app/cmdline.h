/*
 * cmdline.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef CMDLINE_H
#define CMDLINE_H

#include <stdint.h>

namespace ANSI_EscapeCodes {
    namespace Colors {
        static const char* red                         = "\033[31m";
        static const char* brightRed                   = "\033[31;1m";
        static const char* green                       = "\033[32m";
        static const char* brightGreen                 = "\033[32;1m";
        static const char* yellow                      = "\033[33m";
        static const char* brightYellow                = "\033[33;1m";
        static const char* blue                        = "\033[34m";
        static const char* brightBlue                  = "\033[34;1m";
        static const char* magenta                     = "\033[35m";
        static const char* brightMagenta               = "\033[35;1m";
        static const char* cyan                        = "\033[36m";
        static const char* brightCyan                  = "\033[36;1m";
    }
    namespace Decorations {
        static const char* bold                        = "\033[1m";
        static const char* underline                   = "\033[4m";
        static const char* inverted                    = "\033[7m";
    }
    namespace Controls {
        static const char* clearEntireScreen           = "\033[2J";
        static const char* clearScreenBeforeCursor     = "\033[1J";
        static const char* clearScreenAfterCursor      = "\033[0J";
        static const char* clearEntireLine             = "\033[2K";
        static const char* clearLineBeforeCursor       = "\033[1K";
        static const char* clearLineAfterCursor        = "\033[0K";
        static const char* cursorScreenOrigins         = "\033[H";
    }
    namespace CursorNavigation {
        static const char* left                        = "\033[D";
        static const char* right                       = "\033[C";
        static const char* up                          = "\033[A";
        static const char* down                        = "\033[B";
    }

    static const char* reset                           = "\033[0m";
}


extern "C" void taskCmdLine(void*);

#endif /* CMDLINE_H */

