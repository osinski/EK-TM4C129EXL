/*
 * sensor.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef SENSOR_H
#define SENSOR_H

#include <array>
#include <random>
#include <cstring>
#include <algorithm>

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"

struct FakeSensor
{
    FakeSensor(unsigned lowerLimit, unsigned upperLimit) :
        lower {lowerLimit}, upper{upperLimit}
    {}

    std::array<uint16_t, 10> values = {0,0,0,0,0,0,0,0,0,0};
    std::string printableValues;

    unsigned lower, upper;
    
    void getNewValue()
    {
        static std::default_random_engine generator;
        static std::uniform_int_distribution<unsigned> distribution(lower, upper);

        auto newVal = distribution(generator);
        memmove(values.data(), values.data()+1, sizeof(uint16_t)*(values.size()-1));
        values.back() = newVal;
    }

    void makePrintable()
    {
        printableValues.clear();

        for (auto const& val : values) {
            if (&val != &values[0]) {
                printableValues += std::string(",");
            }
            printableValues += std::to_string(val);
        }

    }
};


extern "C" void taskTemperatureSensor(void*);
extern "C" void taskPressureSensor(void*);

#endif /* SENSOR_H */

