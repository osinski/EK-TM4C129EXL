/*
 * main.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include <stdint.h>
#include <utility>
#include <array>
#include <string.h>

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/task.h"
#include "../freeRTOS/include/queue.h"
#include "../freeRTOS/include/semphr.h"
#include "FreeRTOSConfig.h"

#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/inc/hw_types.h"
#include "../tivaWare/inc/hw_gpio.h"
#include "../tivaWare/inc/tm4c129encpdt.h"
#include "../tivaWare/driverlib/gpio.h"
#include "../tivaWare/driverlib/pin_map.h"
#include "../tivaWare/driverlib/interrupt.h"
#include "../tivaWare/driverlib/rom.h"
#include "../tivaWare/driverlib/rom_map.h"
#include "../tivaWare/driverlib/sysctl.h"
#include "../tivaWare/driverlib/uart.h"
#include "../tivaWare/driverlib/usb.h"
#include "../tivaWare/driverlib/flash.h"
#include "../tivaWare/utils/lwiplib.h"
#include "../tivaWare/utils/locator.h"
#include "../tivaWare/usblib/usblib.h"
#include "../tivaWare/usblib/usbcdc.h"
#include "../tivaWare/usblib/usb-ids.h"
#include "../tivaWare/usblib/device/usbdevice.h"
#include "../tivaWare/usblib/device/usbdcdc.h"
#include "usb_structs.h"

#include "../tivaWare/lwip/src/include/lwip/ip4_addr.h"

#include "led.h"
#include "button.h"
#include "uart.h"
#include "cmdline.h"
#include "logger.h"
#include "httpd_server.h"
#include "sensor.h"

#include "metrologia.h"


void configHW();

bool g_bUSBConfigured = false;

constexpr uint32_t adcBufLen = 1024;
uint16_t bufTxADC[adcBufLen];
uint16_t bufRxADC[adcBufLen];

uint8_t uDMAcontrolTable[1024] __attribute__ ((aligned(1024)));

UART uart(UART0_BASE, 9600);
UART darek(UART6_BASE, 9600);

Button button1(GPIO_PORTJ_BASE, GPIO_PIN_0);
Button button2(GPIO_PORTJ_BASE, GPIO_PIN_1);

QueueHandle_t queueLogger = xQueueCreate(1, sizeof(LoggingEvents));
QueueHandle_t queueCmdLine = xQueueCreate(1, sizeof(char));

SemaphoreHandle_t button1Semaphore = xSemaphoreCreateBinary();
SemaphoreHandle_t button2Semaphore = xSemaphoreCreateBinary();


extern "C" int main()
{
    configHW();

    uart.sendString(ANSI_EscapeCodes::Controls::clearEntireScreen);
    uart.sendString(ANSI_EscapeCodes::Controls::cursorScreenOrigins);
    uart.sendString(ANSI_EscapeCodes::Decorations::bold);
    uart.sendString(ANSI_EscapeCodes::Colors::brightCyan);
    uart.sendString("EK-TM4C129EXL Devboard RTOS demo\r\n");
    uart.sendString(ANSI_EscapeCodes::reset);

    xTaskCreate(taskLEDs, "LEDs", 100, NULL, 0, NULL);
    xTaskCreate(taskLogger, "Logger", 100, NULL, 1, NULL);
    xTaskCreate(taskCmdLine, "CmdLine", 1000, NULL, 2, NULL);
    xTaskCreate(taskHTTPServer, "http server", 1000, NULL, 0, NULL);
    xTaskCreate(taskTemperatureSensor, "temp sensor", 1000, NULL, 0, NULL);
    xTaskCreate(taskPressureSensor, "pressure sensor", 1000, NULL, 0, NULL);
    //xTaskCreate(taskMetrologia, "metrologia", 32, 0, 0, NULL);
    vTaskStartScheduler();

    while(1) {};
}

void configHW()
{
    SysCtlMOSCConfigSet(SYSCTL_MOSC_HIGHFREQ);
    SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                        SYSCTL_OSC_MAIN |
                        SYSCTL_USE_PLL |
                        SYSCTL_CFG_VCO_240), configCPU_CLOCK_HZ);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOC));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOJ));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOL));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPION));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOP));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOQ));

    SysCtlPeripheralEnable(SYSCTL_PERIPH_USB0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_USB0));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1));
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UDMA));

    for (auto i = 0; i < userLEDs.size(); i++) {
        userLEDs[i].setup();
    }

    button1.setup();
    button2.setup();

    IntPrioritySet(INT_GPIOJ, 0xE0);
    IntEnable(INT_GPIOJ);
    IntPrioritySet(INT_UART0, 0xE0);
    IntEnable(INT_UART0);
    IntMasterEnable();

    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    uart.setup();

    GPIOPinTypeUART(GPIO_PORTP_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinConfigure(GPIO_PP0_U6RX);
    GPIOPinConfigure(GPIO_PP1_U6TX);
    darek.setup();
    
    GPIOPinConfigure(GPIO_PD6_USB0EPEN);
    GPIOPinTypeUSBAnalog(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinTypeUSBDigital(GPIO_PORTD_BASE, GPIO_PIN_6);
    GPIOPinTypeUSBAnalog(GPIO_PORTL_BASE, GPIO_PIN_6 | GPIO_PIN_7);
    GPIOPinTypeGPIOInput(GPIO_PORTQ_BASE, GPIO_PIN_4);

    USBBufferInit(&g_sTxBuffer);
    USBBufferInit(&g_sRxBuffer);

    uint32_t ui32PLLRate;
    uint32_t sysClock = configCPU_CLOCK_HZ;

    SysCtlVCOGet(SYSCTL_XTAL_25MHZ, &ui32PLLRate);
    USBDCDFeatureSet(0, USBLIB_FEATURE_CPUCLK, &sysClock);
    USBDCDFeatureSet(0, USBLIB_FEATURE_USBPLL, &ui32PLLRate);

    USBStackModeSet(0, eUSBModeForceDevice, 0);

    USBDCDCInit(0, &g_sCDCDevice);


    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);

    uint32_t user0, user1;
    uint8_t MACArray[8];
    FlashUserGet(&user0, &user1);

    if ((user0 == 0xffffffff) || (user1 == 0xffffffff)) {
        uart.sendString("No MAC programmed\r\n");

        while (true);
    }

    MACArray[0] = ((user0 >>  0) & 0xff);
    MACArray[1] = ((user0 >>  8) & 0xff);
    MACArray[2] = ((user0 >> 16) & 0xff);
    MACArray[3] = ((user1 >>  0) & 0xff);
    MACArray[4] = ((user1 >>  8) & 0xff);
    MACArray[5] = ((user1 >> 16) & 0xff);

    uint32_t ipAddr = 192 << 24 | 168 << 16 |   8 << 8 |  24 << 0;
    uint32_t ipGW   = 192 << 24 | 168 << 16 |   8 << 8 |   1 << 0;
    uint32_t ipMask = 255 << 24 | 255 << 16 | 255 << 8 |   0 << 0;

    lwIPInit(configCPU_CLOCK_HZ, MACArray, ipAddr, ipMask, ipGW, IPADDR_USE_STATIC);

}

extern "C" {

void ISR_GPIOJ(void)
{
    static uint64_t lastISR = 0;
    TickType_t ticksISR = xTaskGetTickCount();

    auto xHigherPriorityTaskWoken = pdFALSE;
    LoggingEvents event;

    auto intStatus = GPIOIntStatus(GPIO_PORTJ_BASE, GPIO_INT_PIN_0 |
                                                    GPIO_INT_PIN_1);
    GPIOIntClear(GPIO_PORTJ_BASE, GPIO_INT_PIN_0 | GPIO_INT_PIN_1);
    if (ticksISR - lastISR > pdMS_TO_TICKS(150)) {
        switch (intStatus) {
        case GPIO_INT_PIN_0:
            event = Button1;
            xQueueSendFromISR(queueLogger, &event, &xHigherPriorityTaskWoken);
            break;

        case GPIO_INT_PIN_1:
            event = Button2;
            xQueueSendFromISR(queueLogger, &event, &xHigherPriorityTaskWoken);
            break;

        default:
            break;
        };

        if (xHigherPriorityTaskWoken != pdFALSE) {
            portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
        }
    }

    lastISR = ticksISR;
}

void ISR_UART0(void)
{
    char recvChar;
    auto xHigherPriorityTaskWoken = pdFALSE;

    auto intStatus = UARTIntStatus(UART0_BASE, false);
    UARTIntClear(UART0_BASE, intStatus);

    switch (intStatus) {
    case UART_INT_RT:
        recvChar = UARTCharGet(UART0_BASE);
        xQueueSendFromISR(queueCmdLine, &recvChar, &xHigherPriorityTaskWoken);
        break;

    default:
        break;
    }

    if (xHigherPriorityTaskWoken != pdFALSE) {
        portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
    }
}

uint32_t TxHandler(void* pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue, 
                   void* pvMsgData)
{
    return 0;
}

uint32_t RxHandler(void* pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
                   void* pvMsgData)
{
    return 0;
}

uint32_t ControlHandler(void* pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
                        void* pvMsgData)
{
    //
    // Which event are we being asked to process?
    //
    switch(ui32Event)
    {
        //
        // We are connected to a host and communication is now possible.
        //
        case USB_EVENT_CONNECTED:
            g_bUSBConfigured = true;

            //
            // Flush our buffers.
            //
            USBBufferFlush(&g_sTxBuffer);
            USBBufferFlush(&g_sRxBuffer);

            break;

        //
        // The host has disconnected.
        //
        case USB_EVENT_DISCONNECTED:
            g_bUSBConfigured = false;

            break;

        //
        // Return the current serial communication parameters.
        //
        case USBD_CDC_EVENT_GET_LINE_CODING: {
            tLineCoding* lc = reinterpret_cast<tLineCoding*>(pvMsgData);
            lc->ui32Rate = 115200;
            lc->ui8Databits = 8;
            lc->ui8Parity = USB_CDC_PARITY_NONE;
            lc->ui8Stop = USB_CDC_STOP_BITS_1;
            break;
        }

        //
        // Set the current serial communication parameters.
        //
        case USBD_CDC_EVENT_SET_LINE_CODING:
            //SetLineCoding(pvMsgData);
            break;

        //
        // Set the current serial communication parameters.
        //
        case USBD_CDC_EVENT_SET_CONTROL_LINE_STATE:
            //SetControlLineState((uint16_t)ui32MsgValue);
            break;

        //
        // Send a break condition on the serial line.
        //
        case USBD_CDC_EVENT_SEND_BREAK:
            break;

        //
        // Clear the break condition on the serial line.
        //
        case USBD_CDC_EVENT_CLEAR_BREAK:
            break;

        //
        // Ignore SUSPEND and RESUME for now.
        //
        case USB_EVENT_SUSPEND:
        case USB_EVENT_RESUME:
            break;

        //
        // We don't expect to receive any other events.  Ignore any that show
        // up in a release build or hang in a debug build.
        //
        default:
#ifdef DEBUG
            while(1);
#else
            break;
#endif

    }

    return(0);
}

}
