/*
 * uart.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "logger.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"

#include "uart.h"

extern UART uart;
extern QueueHandle_t queueLogger;

extern "C" void taskLogger(void*)
{
    LoggingEvents events;

    while (1) {
        xQueueReceive(queueLogger, &events, portMAX_DELAY);

        switch (events) {
        case Button1:
            uart.sendString("Button1 pressed\r\n");
            break;

        case Button2:
            uart.sendString("Button2 pressed\r\n");
            break;
        }

    }
}
