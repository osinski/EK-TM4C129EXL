/*
 * metrologia.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "metrologia.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/task.h"

#include "uart.h"

extern UART darek;
extern UART uart;

void taskMetrologia(void*)
{
    static char recvBuf[10];
    const char* msg = "KAJAK";

    while(1) {
        darek.sendString(msg);

        for (size_t i = 0; i < strlen(msg); i++) {
            recvBuf[i] = UARTCharGetNonBlocking(darek.periph);
        }

        uart.flushBuffer(recvBuf, 5);
        uart.sendString("\r\n");

        memset(recvBuf, 0, sizeof(recvBuf));

        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

