/*
 * uart.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef UART_H
#define UART_H

#include <stdint.h>
#include <string.h>

#include "FreeRTOSConfig.h"
#include "../tivaWare/inc/tm4c129encpdt.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/driverlib/gpio.h"
#include "../tivaWare/driverlib/uart.h"


struct UART
{
    UART(const uint32_t uartPeriph, const uint32_t uartBaudrate)
        : periph {uartPeriph}, baudrate {uartBaudrate}
    {}

    const uint32_t periph;
    const uint32_t baudrate;

    void setup()
    {
        UARTConfigSetExpClk(periph, configCPU_CLOCK_HZ, baudrate,
                            UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | 
                            UART_CONFIG_PAR_NONE);
        UARTIntEnable(periph, UART_INT_RX | UART_INT_RT);
    }

    void sendChar(const char c)
    {
        UARTCharPut(periph, c);
    }

    void sendString(const char* str)
    {
        while (*str != '\0') {
            sendChar(*str++);
        }
    }

    void flushBuffer(const char* buf, size_t len)
    {
        for (size_t i = 0; i < len; i++) {
            sendChar(buf[i]);
        }
    }
};


#endif /* UART_H */

