window.onload = function() {
    document.getElementById('about').onclick = loadAbout;
    document.getElementById('overview').onclick = loadOverview;
    document.getElementById('ctrl').onclick = loadControl;

    loadPage("about.html")
    document.getElementById('plotTemperature').style.display = "none";
    document.getElementById('plotPressure').style.display = "none";

    window.setInterval(updatePlots, 5000);
}

function loadAbout() {
    loadPage("about.html");
    document.getElementById('plotTemperature').style.display = "none";
    document.getElementById('plotPressure').style.display = "none";
    return false;
}

function loadOverview() {
    loadPage("overview.html");
    document.getElementById('plotTemperature').style.display = "none";
    document.getElementById('plotPressure').style.display = "none";
    return false;
}

function loadControl() {
    loadPage("control.html");
    document.getElementById('plotTemperature').style.display = "inline-block";
    document.getElementById('plotPressure').style.display = "inline-block";
    return false;
}

function updatePlots() {
    var reqTemp = false;
    var reqPressure = false;
    var recvData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


    if (window.XMLHttpRequest) {
        reqTemp = new XMLHttpRequest();
        reqPressure = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        reqTemp = new ActiveXObject("Microsoft.XMLHTTP");
        reqPressure = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (reqTemp) {
        reqTemp.open("GET", "/temperatureData?", true);
        reqTemp.onreadystatechange = function () {
            if ((reqTemp.readyState == 4) && (reqTemp.status == 200)) {
                recvData = reqTemp.responseText.split(",").map(Number);
                Plotly.restyle(plotTemperatureDiv, {y: [recvData]});
            }
        }
        reqTemp.send(null);
    }
    if (reqPressure) {
        reqPressure.open("GET", "/pressureData?", true);
        reqPressure.onreadystatechange = function () {
            if ((reqPressure.readyState == 4) && (reqPressure.status == 200)) {
                recvData = reqPressure.responseText.split(",").map(Number);
                Plotly.restyle(plotPressureDiv, {y: [recvData]});
            }
        }
        reqPressure.send(null);
    }
}

function loadPage(page) {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET", page, true);
    xmlhttp.setRequestHeader("Content-type",
        "application/x-www-form-urlencoded");
    xmlhttp.send();

    xmlhttp.onreadystatechange = function() {
        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
            document.getElementById("content").innerHTML = xmlhttp
                .responseText;
        }
    }
}
