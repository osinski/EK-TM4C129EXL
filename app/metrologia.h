/*
 * metrologia.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef METROLOGIA_H
#define METROLOGIA_H


extern "C" void taskMetrologia(void*);


#endif /* METROLOGIA_H */

