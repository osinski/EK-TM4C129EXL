/*
 * debug.c
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include <stdint.h>

#ifdef DEBUG
// Used internally by driverlib
void __error__(char *pcFilename, uint32_t ui32Line)
{
    while(1);
}
#endif
