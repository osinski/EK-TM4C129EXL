/*
 * httpd_server.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "httpd_server.h"

#include <cstring>
#include <cstdio>
#include <string>
#include <string_view>
#include <optional>
#include <array>

#include "../tivaWare/lwip/src/include/lwip/api.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/task.h"
#include "../freeRTOS/include/queue.h"
#include "../freeRTOS/include/stream_buffer.h"

#include "uart.h"
#include "fs.h"
#include "httpd.h"
#include "fsdata.c"


class http_header_get
{
    std::string_view requested_file;
    std::string_view http_version;

    constexpr static std::string_view default_file = "/index.html";

    http_header_get() = default;

public:
    static std::optional<http_header_get>
    build_http(const std::string_view data)
    {
        http_header_get hdr;
        auto nline_pos = data.find("\n");

        if (nline_pos == std::string::npos) {
            return std::nullopt;
        }

        auto fline = data.substr(0, nline_pos);

        if (!fline.starts_with("GET")) {
            return std::nullopt;
        }

        auto file_pos_beg = fline.find("/");
        auto file_pos_end = fline.find(" ", file_pos_beg);

        if(file_pos_beg == std::string::npos || file_pos_end == std::string::npos)
        {
            return std::nullopt;
        }

        hdr.requested_file = 
                fline.substr(file_pos_beg, file_pos_end - file_pos_beg);
        hdr.http_version = 
                fline.substr(file_pos_end, nline_pos-file_pos_end-1);

        if(hdr.requested_file.size() == 1 and *hdr.requested_file.data() == '/') {
            hdr.requested_file = default_file;
        }

        return hdr;
    }

    auto
    get_file()
    {
        return requested_file;
    }

    auto
    get_version()
    {
        return http_version;
    }
};

template<typename T, std::size_t U>
static std::string makePrintable(const std::array<T, U> &data);

static void http_server_netconn_serve(struct netconn* conn);


extern "C" void taskHTTPServer(void *)
{
    struct netconn *conn, *newconn;
    err_t err;

    /* Create a new TCP connection handle */
    /* Bind to port 80 (HTTP) with default IP address */
#if LWIP_IPV6
    netconn_bind(conn, IP6_ADDR_ANY, 80);
#else /* LWIP_IPV6 */
    conn = netconn_new(NETCONN_TCP);
    netconn_bind(conn, IP_ADDR_ANY, 80);
#endif /* LWIP_IPV6 */
    if (conn == NULL) while(1);

    /* Put the connection into LISTEN state */
    netconn_listen(conn);

    do {
      err = netconn_accept(conn, &newconn);
      if (err == ERR_OK) {
        http_server_netconn_serve(newconn);
        netconn_delete(newconn);
      }
    } while(err == ERR_OK);

    while(1);
    netconn_close(conn);
    netconn_delete(conn);
}

extern UART uart;
extern StreamBufferHandle_t streamTemp, streamPressure;

/** Serve one HTTP connection accepted in the http thread */
static void http_server_netconn_serve(struct netconn *conn)
{
    struct netbuf *inbuf;
    char *buf;
    u16_t buflen;
    err_t err;

    static char tempValsStr[100];
    static char pressureValsStr[100];

    xStreamBufferReceive(streamTemp, tempValsStr, 100, 0);
    xStreamBufferReceive(streamPressure, pressureValsStr, 100, 0);

    /* Read the data from the port, blocking if nothing yet there.
     We assume the request (the part we care about) is in one netbuf */
    err = netconn_recv(conn, &inbuf);


    if (err == ERR_OK) {
        netbuf_data(inbuf, (void**)&buf, &buflen);

        auto hOpt = http_header_get::build_http({buf, buflen});

        if(hOpt.has_value()) {
            auto h = *hOpt;

            auto page = h.get_file();

            uart.flushBuffer(page.data(), page.size());
            uart.sendString("\r\n");

            static fs_file file;
 
            auto notFile = fs_open(&file, (std::string(page)+'\0').c_str());

            if (notFile) {
                if (page == std::string_view("/temperatureData?")) {
                    uart.sendString("...Responding with temperature\r\n...");

                    uart.flushBuffer(tempValsStr, strlen(tempValsStr));
                    uart.sendString("\r\n");

                    netconn_write(conn, tempValsStr, strlen(tempValsStr),
                                  NETCONN_NOCOPY);

                    memset(tempValsStr, 0, 100);

                } else if (page == std::string_view("/pressureData?")) {
                    uart.sendString("...Responding with pressure\r\n...");

                    uart.flushBuffer(pressureValsStr, strlen(pressureValsStr));
                    uart.sendString("\r\n");

                    netconn_write(conn, pressureValsStr, strlen(pressureValsStr),
                                  NETCONN_NOCOPY);

                    memset(pressureValsStr, 0, 100);
                }
            } else {
                netconn_write(conn, file.data, file.len, NETCONN_NOCOPY);
            }

            fs_close(&file);
        }
    }
    /* Close the connection (server closes in HTTP) */
    netconn_close(conn);

    /* Delete the buffer (netconn_recv gives us ownership,
     so we have to make sure to deallocate the buffer) */
    netbuf_delete(inbuf);
}

template<typename T, std::size_t U>
static std::string makePrintable(const std::array<T, U> &data)
{
    std::string printable;

    for (auto const& val : data) {
        if (&val != &data[0]) {
            printable += std::string(",");
        }
        printable += std::to_string(val);
    }

    return printable;
}
