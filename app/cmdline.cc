/*
 * cmdline.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "cmdline.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"

#include "logger.h"
#include "uart.h"


extern UART uart;
extern QueueHandle_t queueCmdLine;

extern "C" void taskCmdLine(void*)
{
    char recvChar;
    static char rawBuf[128];
    static char cmdBuf[32];

    static size_t idx = 0;
    static bool escapeCharRecvd = false;
    static size_t escapeCharRecvdIdx = 0;

    static uint8_t leftArrowsCount = 0;

    while (1) {
        xQueueReceive(queueCmdLine, &recvChar, portMAX_DELAY);

        rawBuf[idx++] = recvChar;

        switch(recvChar) {
        case '\r':
            uart.sendString("\r\n");
            leftArrowsCount = 0;
            // get command and execute it
            uart.sendString(ANSI_EscapeCodes::Colors::green);
            uart.sendString("cmdBuf:\r\n");
            uart.sendString(ANSI_EscapeCodes::reset);
            memcpy(cmdBuf, rawBuf, idx);
            uart.flushBuffer(cmdBuf, idx);
            uart.sendString("\r\n");
            idx = 0;
            break;

        case '\010':
        case 127:
            uart.sendString(ANSI_EscapeCodes::CursorNavigation::left);
            uart.sendString(ANSI_EscapeCodes::Controls::clearLineAfterCursor);

            break;

        case '\033':
            escapeCharRecvd = true;
            break;

        default:
            if (!escapeCharRecvd) {
                uart.sendChar(recvChar);
            }
        }

        if (escapeCharRecvd) {
            idx--;
            escapeCharRecvdIdx++;
            if (escapeCharRecvdIdx == 3) {
                escapeCharRecvdIdx = 0;
                escapeCharRecvd = false;

                if (recvChar == 'D' && idx > 0) {
                    idx--;
                    leftArrowsCount++;
                    uart.sendString(ANSI_EscapeCodes::CursorNavigation::left);
                } else if (recvChar == 'C' && leftArrowsCount != 0) {
                    idx++;
                    leftArrowsCount--;
                    uart.sendString(ANSI_EscapeCodes::CursorNavigation::right);
                }
            }
        }
    }
}

