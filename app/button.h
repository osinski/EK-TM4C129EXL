/*
 * button.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef BUTTON_H
#define BUTTON_H

#include <stdint.h>

#include "../tivaWare/inc/tm4c129encpdt.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/driverlib/gpio.h"


struct Button
{
    Button(const uint32_t buttonPort, const uint32_t buttonPin) :
        port {buttonPort}, pin {buttonPin}
    {}

    const uint32_t port;
    const uint32_t pin;

    void setup()
    {
        GPIOPinTypeGPIOInput(port, pin);
        GPIOPadConfigSet(port, pin, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
        GPIOIntTypeSet(port, pin, GPIO_FALLING_EDGE);
        GPIOIntEnable(port, pin);
    }

};

#endif /* BUTTON_H */

