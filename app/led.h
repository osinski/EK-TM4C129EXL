/*
 * led.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef LED_H
#define LED_H

#include <stdint.h>
#include <array>

#include "../tivaWare/inc/tm4c129encpdt.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/driverlib/gpio.h"

struct LED
{
    LED(const uint32_t ledport, const uint32_t ledpin) :
        port {ledport}, pin {ledpin}
    {};

    const uint32_t port;
    const uint32_t pin;

    void setup()
    {
        GPIOPinTypeGPIOOutput(port, pin);
    }

    void enable()
    {
        GPIOPinWrite(port, pin, pin);
    }

    void disable()
    {
        GPIOPinWrite(port, pin, 0);
    }
};

extern "C" void taskLEDs(void*);

extern std::array<LED, 4> userLEDs;


#endif /* LED_H */

