/*
 * logger.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef LOGGER_H
#define LOGGER_H


enum LoggingEvents
{
    Button1,
    Button2,
};


extern "C" void taskLogger(void*);


#endif /* LOGGER_H */

