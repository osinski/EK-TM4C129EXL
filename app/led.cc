/*
 * led.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "led.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/task.h"

std::array<LED, 4> userLEDs {
    LED(GPIO_PORTN_BASE, GPIO_PIN_1),
    LED(GPIO_PORTN_BASE, GPIO_PIN_0),
    LED(GPIO_PORTF_BASE, GPIO_PIN_4),
    LED(GPIO_PORTF_BASE, GPIO_PIN_0),
};

void taskLEDs(void*)
{
    while (1) {
        for (auto& led : userLEDs) {
            led.enable();
            vTaskDelay(pdMS_TO_TICKS(100));
        }

        for (auto& led : userLEDs) {
            led.disable();
            vTaskDelay(pdMS_TO_TICKS(100));
        }
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}
