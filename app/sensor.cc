/*
 * sensor.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "sensor.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/task.h"
#include "../freeRTOS/include/stream_buffer.h"

#include "uart.h"

extern UART uart;

static constexpr unsigned refreshTime = 5000;

StreamBufferHandle_t streamTemp, streamPressure;

FakeSensor tempSensor{10, 30};
FakeSensor pressureSensor{1000, 1020};

extern "C" void taskTemperatureSensor(void*)
{
    streamTemp = xStreamBufferCreate(100, 15);

    while (true) {
        vTaskDelay(pdMS_TO_TICKS(refreshTime));

        tempSensor.getNewValue();
        tempSensor.makePrintable();

        xStreamBufferSend(streamTemp, tempSensor.printableValues.c_str(),
                          tempSensor.printableValues.length(), 0);

    }
}

extern "C" void taskPressureSensor(void*)
{
    streamPressure = xStreamBufferCreate(100, 15);

    while (true) {
        vTaskDelay(pdMS_TO_TICKS(refreshTime));

        pressureSensor.getNewValue();

        pressureSensor.values.back() += 990;

        pressureSensor.makePrintable();

        xStreamBufferSend(streamPressure, pressureSensor.printableValues.c_str(),
                          pressureSensor.printableValues.length(), 0);
    }
}
